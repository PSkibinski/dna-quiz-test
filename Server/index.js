const express = require("express");
var data = require("./files/db.json");
const fs = require("fs");
const PORT = 4000;
const app = express();

app.use((req, res, next) => {
    res.append("Access-Control-Allow-Origin", ["*"]);
    res.append("Access-Control-Allow-Methods", "GET");
    res.append("Access-Control-Allow-Headers", "Content-Type");
    next();
});

app.use(express.json());
app.use(
    express.urlencoded({
        extended: true,
    })
);

app.use(express.static("files"));

app.get("/", (req, res) => {
    res.send(data[req.query.name]);
});

app.get("/keys", (req, res) => {
    res.send(Object.keys(data));
});

app.post("/addNewTask", (req, res) => {
    data[Object.keys(req.body)[0]] = req.body[Object.keys(req.body)];
    fs.writeFileSync("./files/db.json", JSON.stringify(data));
    res.sendStatus(200);
});

app.listen(PORT, () => console.log(`http://localhost:${PORT}`));
