const express = require("express");
const PORT = 3000;
const app = express();

app.use(express.static("public"));
app.use(express.static("/node_modules"));

app.get("/", function (req, res) {
    res.render("./public/index.html");
});

app.listen(PORT, () => console.log(`http://localhost:${PORT}`));
