const app = angular.module("app", ["questions", "config", "addNewTest"]);

app.run($rootScope => {
    $rootScope.selectedTest = "";
});

app.controller(
    "AppController",
    ($scope, $rootScope, PAGES, QuestionsService) => {
        $scope.pages = PAGES;

        $scope.currentPage = $scope.pages[0];
        $scope.test = "";
        $scope.allTest = [];

        QuestionsService.getTestKeys()
            .then(res => ($scope.allTest = res.data))
            .then(() => {
                $scope.test = $scope.allTest[0];
                $rootScope.selectedTest = $scope.allTest[0];
            });

        $scope.updateSelectedTest = item => {
            $rootScope.selectedTest = item;
            $scope.test = $rootScope.selectedTest;
        };

        $scope.goToPage = pageName => {
            $scope.currentPage = $scope.pages.find(p => p.name === pageName);
        };
        $scope.goToPage("main");
    }
);

angular.bootstrap(document, ["app"]);
