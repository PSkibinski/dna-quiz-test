const addNewTest = angular.module('addNewTest', ['questions.service']);

addNewTest.controller('AddNewTestController', ($scope, QuestionsService) => {
    const vm = ($scope.addNewTestScope = {});

    vm.newTest = {};
    vm.name = null;
    vm.question = null;
    vm.answer1 = null;
    vm.answer2 = null;
    vm.answer3 = null;
    vm.correctAnswer = null;

    vm.addNewTest = (name, question, answers, correctAnswer) => {
        vm.newTest[name] = [
            {
                id: 1,
                question,
                answers,
                correctAnswer,
            },
        ];

        console.log(vm.newTest);

        return vm.newTest;
    };
});
