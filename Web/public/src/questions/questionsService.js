const questionsService = angular.module("questions.service", ["config"]);

questionsService.service("QuestionsService", function ($http, SERVICES) {
    this.url = SERVICES.find(res => res.name === "tests").url;

    this.getQuestions = selectedTest => {
        let filtr = "?name=";
        return $http.get(this.url + filtr + selectedTest);
    };

    this.getTestKeys = () => {
        let filtr = "keys";
        return $http.get(this.url + filtr);
    };

    this.pushTest = data => {
        let filtr = "addNewTask/";
        return $http.post(this.url + filtr, data);
    };
});
