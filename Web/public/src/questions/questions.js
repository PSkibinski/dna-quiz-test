const questions = angular.module("questions", ["questions.service"]);

questions.controller(
    "QuestionsController",
    ($scope, $rootScope, QuestionsService) => {
        const vm = ($scope.questionsScope = {
            data: [],
        });

        vm.status = "loading";
        vm.currentQuestion = 0;

        QuestionsService.getQuestions($rootScope.selectedTest)
            .then(res => (vm.data = res.data))
            .then(() => {
                vm.questionsLenght = vm.data.length - 1;
                vm.answers = [];
                vm.status = "ready";
            });

        vm.nextQuestion = () => {
            vm.currentQuestion++;
            if (vm.currentQuestion > vm.questionsLenght) vm.status = "saving";
        };

        vm.previousQuestion = () => {
            vm.currentQuestion--;
        };

        vm.selectAnswer = answer => {
            vm.answers[vm.currentQuestion] = answer;
            vm.nextQuestion();
        };

        vm.changeQuestions = () => {
            vm.status = "ready";
            vm.currentQuestion = 0;
        };

        vm.sendAnswers = () => {
            vm.sumOfCorrectAnswers = vm.data.reduce((sum, item, index) => {
                if (item.correctAnswer === vm.answers[index]) return ++sum;
                return sum;
            }, 0);
            vm.status = "end";
        };
    }
);
