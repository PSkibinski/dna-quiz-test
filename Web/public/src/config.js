const config = angular.module("config", []);

config.constant("SERVICES", [
    {
        name: "tests",
        url: "http://localhost:4000/",
    },
]);

config.constant("PAGES", [
    {
        name: "main",
        url: "./views/main.html",
    },
    {
        name: "questions",
        url: "./views/questions.html",
    },
    {
        name: "addNewTest",
        url: "./views/addNewTest.html",
    },
]);
